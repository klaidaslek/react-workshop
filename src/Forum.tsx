import React, { useState } from 'react';
import Typography from '@revel/core/generic/Typography';

import ForumHeader from './components/ForumHeader';
import ForumThread from './components/ForumThread';
import ForumTheadNew from './components/ForumTheadNew';
import { Theme, WithStyles } from '@revel/core/types';
import { createStyles, withStyles } from '@revel/core/styles';


const styles = (theme: Theme) => createStyles({
    threadsContainer: {
        display: 'flex',
        flexDirection: 'column',
        paddingLeft: theme.spacing.unit * 5,
        paddingRight: theme.spacing.unit * 5,
        height: theme.spacing.unit * 50,
        overflowY: 'scroll'
    }
});


export interface Thread {
    name: string;
}

interface ForumProps extends WithStyles<typeof styles> {

}

const Forum: React.FunctionComponent<ForumProps> = ({classes}) => {

    const [threads, addThread] = useState<Thread[]>([]);
    const [isLoading, setIsLoading] = useState(false);


    const addNewThead = (name: string) => {
        setIsLoading(true);
        setTimeout(() => {
            addThread([...threads, {
                name: name,
            }])
            setIsLoading(false);
        }, 2000);
    };

    return (
        <>
            <ForumHeader
                isNewThreadLoading={isLoading}
            />
            <div className={classes.threadsContainer}>
                {threads.length <= 0 ?
                    <Typography variant='h5'>No threads yet...</Typography> :
                    threads.map((thread, index) => <ForumThread key={index} name={thread.name}/>)
                }
            </div>
            <ForumTheadNew onClick={addNewThead}/>
        </>
    );
};


export default withStyles(styles)(Forum);
