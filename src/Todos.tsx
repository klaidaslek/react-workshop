import React, { useState } from 'react';

const Todos: React.FunctionComponent = () => {
  const [todos, setTodos] = useState<string[]>([]);
  const [inputValue, changeInputValue] = useState('');

  const addNewTodo = (value: string) => {
    setTodos([
        ...todos,
        value
    ]);
    changeInputValue('');
  };

  return (
      <>
        <h1>My Todos</h1>
        {todos.length > 0 ? todos.map((todo, index) => <h5 key={index}>{todo}</h5>) : <h2>Oh my, you have no todos...</h2>}
        <input onChange={event => changeInputValue(event.target.value)} value={inputValue} />
        <button onClick={() => addNewTodo(inputValue)}>Add todo!</button>
      </>
  );
};

export default Todos;
