import React from 'react';


type TodoClassProps = {}
type TodoClassState = {
    todos: string[];
    inputValue: string;
}


class TodosClass extends React.Component<TodoClassProps, TodoClassState> {
    constructor(props: TodoClassProps) {
        super(props);
        this.state = {
            todos: [],
            inputValue: ''
        }
    }

    addNewTodo = (value: string) => {
        this.setState((state) => {
            return {todos: [...state.todos, value], inputValue: ''}
        })
    };

    render() {
        const { todos, inputValue } = this.state;
        return (
            <>
                <h1>My Todos</h1>
                {todos.length > 0 ? todos.map((todo, index) => <h5 key={index}>{todo}</h5>) :
                    <h2>Oh my, you have no todos...</h2>}
                <input onChange={event => this.setState({ inputValue: event.target.value })} value={inputValue}/>
                <button onClick={() => this.addNewTodo(inputValue)}>Add todo!</button>
            </>
        );
    }
}

export default TodosClass;
