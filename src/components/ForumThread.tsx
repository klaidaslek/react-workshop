import React, { useState } from 'react';
import { Thread } from '../Forum';
import TextField from '@revel/core/generic/TextField';
import Button from '@revel/core/generic/Button';

const ForumThread: React.FunctionComponent<Thread> = ({
    name,
}) => {
    const [posts, addPost] = useState<string[]>([]);
    const [postValue, changePostValue] = useState('');

    const renderPosts = () => posts.map(post => <p>{post}</p>);

    const addNewPost = (text: string) => {
        addPost([...posts, text]);
        changePostValue('');
    };

    return(
        <>
            <h2>Name: {name}</h2>
            <h3>Posts</h3>
            {posts.length <= 0 ? <i>No posts yet...</i> : renderPosts()}
            <h3>Add new post</h3>
            <TextField
                onChange={event => changePostValue(event.target.value)}
                value={postValue}
                label={'Add your comment here...'}
            />
            <Button onClick={() => addNewPost(postValue)}>Submit Post</Button>
        </>
    )
};

export default ForumThread;
