import React from 'react';
import CircularProgress from '@revel/core/generic/CircularProgress';
import AppBar from '@revel/core/generic/AppBar';
import Typography from '@revel/core/generic/Typography';
import { createStyles, withStyles } from '@revel/core/styles'
import { WithStyles } from '@revel/core/types';

const styles = () =>
    createStyles({
        root: {
            alignItems: 'center'
        }
    });

interface ForumHeaderProps extends WithStyles<typeof styles> {
    isNewThreadLoading: boolean;
}

const ForumHeader: React.FunctionComponent<ForumHeaderProps> = ({ classes, isNewThreadLoading }) => {
    return (
        <AppBar position='sticky' color='secondary' className={classes.root}>
            { isNewThreadLoading ?
                <CircularProgress /> :
                <Typography variant='h2'>Revel Forum!</Typography>
            }

            <Typography variant='h4'>We make history here...</Typography>
        </AppBar>
    );
};


export default withStyles(styles)(ForumHeader);
