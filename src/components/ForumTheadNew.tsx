import React, { useState } from 'react';
import Button from '@revel/core/generic/Button';
import Typography from '@revel/core/generic/Typography';
import TextField from '@revel/core/generic/TextField';
import { Theme, WithStyles } from '@revel/core/types';
import { createStyles, withStyles } from '@revel/core/styles';


const styles = (theme: Theme) => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        padding: theme.spacing.unit * 10
    }
});


interface ForumTheadNewProps extends WithStyles<typeof styles> {
    onClick: (name: string) => void;
}


const ForumTheadNew: React.FunctionComponent<ForumTheadNewProps> = ({onClick, classes}) => {
    const [threadName, setTheadName] = useState('');

    const submitNewThead = () => {
        onClick(threadName);
        setTheadName('');
    };

    return (
        <div className={classes.root}>
            <Typography variant='h2'>Add new thread</Typography>
            <TextField
                variant='outlined'
                value={threadName}
                onChange={event => setTheadName(event.target.value)}
                label={'Enter your thread name'}
            />
            {threadName.length > 0 && <Button onClick={() => submitNewThead()} variant={'outlined'} color='primary'>Submit</Button>}
        </div>
    );
};

export default withStyles(styles)(ForumTheadNew);
