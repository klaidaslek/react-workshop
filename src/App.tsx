import React from 'react';
import Forum from './Forum';
import TodosClass from './TodosClass';

const App: React.FunctionComponent = () => {
  return (
      <Forum />
  );
};

export default App;
