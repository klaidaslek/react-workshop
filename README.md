
# Hackaton 2019 - React Workshop

## Workshop content

### Create a new project!
* `npm intall npx`
* `npx create-react-app my-app --typescript`
* `cd my-app`
* `npm start`

And you're ready to go :)

### TypeScript
Allows to have optional static type checking
* Each variable or function argument can have type explicitly provided
* If wrong type is passed, warnings/errors are generated within the IDE as well as the TypeScript compiler.
* TypeScript is compiled back to regular JavaScript. After code compilation, there are no types left.

[TypeScript in 5 minutes](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)

### React Hooks
Allows to use all main React features without writing class components.
* It is now possible to provide state to functional components.
* Allows to reuse stateful logic between components (custom hooks)
* `useState` - provides a stateful value and a function to update it.

All components that we'll be writing will be functional components
* Example of a class component and a functional component of `Todos`

### React Forum
Simple component with ability to add new threads and add posts to it.
* Introduction to Revel UI & reusable components library
* Using internal component's state to store new threads and posts

### Issues
* We cannot store everything in local component's state. Well, we can, but then we'll start passing tons of props
to to other components, creating tight coupling between them. What if there would be a global `state` which would allow
every component to "connect" to it? [Redux Motivation](https://redux.js.org/introduction/motivation)
 


## Available Scripts

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Startup instructions
* Make sure to install current version of [Node.js](https://nodejs.org/en/)
* Clone the repository
* Run `npm install`
* Run `npm start`

## Next Time
* Redux - state container
* API calls to retrieve and store data
